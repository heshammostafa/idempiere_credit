package org.egic.factories;

import java.sql.ResultSet;


import org.adempiere.base.IModelFactory;
import org.compiere.model.PO;
import org.compiere.util.Env;
import org.egic.model.MCustomerCreditLimit;

public class MyModelFactory implements IModelFactory {

	@Override
	public Class<?> getClass(String tableName) {
		// TODO Auto-generated method stub
		if(tableName.equalsIgnoreCase(MCustomerCreditLimit.Table_Name))
			return MCustomerCreditLimit.class;
		return null;
	}

	@Override
	public PO getPO(String tableName, int Record_ID, String trxName) {
		// TODO Auto-generated method stub
		if(tableName.equalsIgnoreCase(MCustomerCreditLimit.Table_Name))
			return new MCustomerCreditLimit(Env.getCtx(), Record_ID, trxName);
		return null;
	}

	@Override
	public PO getPO(String tableName, ResultSet rs, String trxName) {
		// TODO Auto-generated method stub
		if(tableName.equalsIgnoreCase(MCustomerCreditLimit.Table_Name))
			return new MCustomerCreditLimit(Env.getCtx(), rs, trxName);
		return null;
	}

}
