package org.egic.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.util.CLogger;

public class MCustomerCreditLimit extends X_C_BPartnerCreditLimit {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4865031023905268568L;
	CLogger log = CLogger.getCLogger(MCustomerCreditLimit.class);

	public MCustomerCreditLimit(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
		log.warning("------------------------------My Model");
	}

	public MCustomerCreditLimit(Properties ctx, int C_BPartnerCreditLimit_ID,
			String trxName) {
		super(ctx, C_BPartnerCreditLimit_ID, trxName);
		// TODO Auto-generated constructor stub
		log.warning("------------------------------My Model");
	}

	@Override
	protected boolean afterSave(boolean newRecord, boolean success) {
		// TODO Auto-generated method stub
		log.warning("------------------------------After Save Event");
		return super.afterSave(newRecord, success);
	}
}
