/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.egic.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for C_BPartnerCreditLimit
 *  @author iDempiere (generated) 
 *  @version Release 2.0 - $Id$ */
public class X_C_BPartnerCreditLimit extends PO implements I_C_BPartnerCreditLimit, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20140618L;

    /** Standard Constructor */
    public X_C_BPartnerCreditLimit (Properties ctx, int C_BPartnerCreditLimit_ID, String trxName)
    {
      super (ctx, C_BPartnerCreditLimit_ID, trxName);
      /** if (C_BPartnerCreditLimit_ID == 0)
        {
			setC_BPartnerCreditLimit_ID (0);
			setC_BPartner_ID (0);
			setM_Product_Category_ID (0);
        } */
    }

    /** Load Constructor */
    public X_C_BPartnerCreditLimit (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 7 - System - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_C_BPartnerCreditLimit[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Business Partener Credit Limit.
		@param C_BPartnerCreditLimit_ID Business Partener Credit Limit	  */
	public void setC_BPartnerCreditLimit_ID (int C_BPartnerCreditLimit_ID)
	{
		if (C_BPartnerCreditLimit_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_BPartnerCreditLimit_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_BPartnerCreditLimit_ID, Integer.valueOf(C_BPartnerCreditLimit_ID));
	}

	/** Get Business Partener Credit Limit.
		@return Business Partener Credit Limit	  */
	public int getC_BPartnerCreditLimit_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartnerCreditLimit_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set C_BPartnerCreditLimit_UU.
		@param C_BPartnerCreditLimit_UU C_BPartnerCreditLimit_UU	  */
	public void setC_BPartnerCreditLimit_UU (String C_BPartnerCreditLimit_UU)
	{
		set_ValueNoCheck (COLUMNNAME_C_BPartnerCreditLimit_UU, C_BPartnerCreditLimit_UU);
	}

	/** Get C_BPartnerCreditLimit_UU.
		@return C_BPartnerCreditLimit_UU	  */
	public String getC_BPartnerCreditLimit_UU () 
	{
		return (String)get_Value(COLUMNNAME_C_BPartnerCreditLimit_UU);
	}

	public org.compiere.model.I_C_BPartner getC_BPartner() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getC_BPartner_ID(), get_TrxName());	}

	/** Set Business Partner .
		@param C_BPartner_ID 
		Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID)
	{
		if (C_BPartner_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_ID, Integer.valueOf(C_BPartner_ID));
	}

	/** Get Business Partner .
		@return Identifies a Business Partner
	  */
	public int getC_BPartner_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Credit Value.
		@param credit_value Credit Value	  */
	public void setcredit_value (BigDecimal credit_value)
	{
		set_Value (COLUMNNAME_credit_value, credit_value);
	}

	/** Get Credit Value.
		@return Credit Value	  */
	public BigDecimal getcredit_value () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_credit_value);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_M_Product_Category getM_Product_Category() throws RuntimeException
    {
		return (org.compiere.model.I_M_Product_Category)MTable.get(getCtx(), org.compiere.model.I_M_Product_Category.Table_Name)
			.getPO(getM_Product_Category_ID(), get_TrxName());	}

	/** Set Product Category.
		@param M_Product_Category_ID 
		Category of a Product
	  */
	public void setM_Product_Category_ID (int M_Product_Category_ID)
	{
		if (M_Product_Category_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_Product_Category_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_Product_Category_ID, Integer.valueOf(M_Product_Category_ID));
	}

	/** Get Product Category.
		@return Category of a Product
	  */
	public int getM_Product_Category_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_Category_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}