package org.egic.component;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.adempiere.base.event.AbstractEventHandler;
import org.adempiere.base.event.IEventTopics;
import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.Query;
import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.egic.model.I_C_BPartnerCreditLimit;
import org.egic.model.MCustomerCreditLimit;
import org.osgi.service.event.Event;

public class CreditDocEvent extends AbstractEventHandler {

	CLogger log = CLogger.getCLogger(CreditDocEvent.class);

	@Override
	protected void initialize() {
		// TODO Auto-generated method stub
		registerTableEvent(IEventTopics.DOC_BEFORE_PREPARE, MOrder.Table_Name);
		registerTableEvent(IEventTopics.PO_BEFORE_NEW, MOrder.Table_Name);
	}

	@Override
	protected void doHandleEvent(Event event) {
		if ( event.getTopic().equals(IEventTopics.PO_BEFORE_NEW)){
			if (getPO(event) instanceof MOrder){
				
				log.warning("------------------New Order ----");
			}
			
		}
		
		if (event.getTopic().equals(IEventTopics.DOC_BEFORE_PREPARE)) {
			if (getPO(event) instanceof MOrder) {
				MOrder m_order = (MOrder) getPO(event);
						
				String whereClause = "C_BPartner_ID="+ m_order.getC_BPartner_ID();
				List<MCustomerCreditLimit> limits = new Query(Env.getCtx(),I_C_BPartnerCreditLimit.Table_Name, whereClause, null).list();
				Map<Integer, BigDecimal> limitMap = new HashMap<Integer, BigDecimal>();
				for (MOrderLine line : m_order.getLines()) {
					Integer category_id = new Integer(line.getM_Product().getM_Product_Category_ID());
					BigDecimal limit = limitMap.get(category_id);
					if (limit == null)
						limit = BigDecimal.ZERO;
					limit = limit.add(line.getLineNetAmt()); 

					limitMap.put(category_id, limit);
					
				}

				for (MCustomerCreditLimit limit : limits) {
					Integer category_id = limit.getM_Product_Category_ID();
					if (limitMap.containsKey(category_id)) {
						BigDecimal limitVal = limitMap.get(category_id);
						BigDecimal myVal = limit.getcredit_value();
												
						log.warning("---Order Value---"+ limitVal + "----Credit Value---" + myVal);
						log.warning("-Compare Results---> "+ limitVal.compareTo(myVal));
						
						if (limitVal!=null&&limitVal.compareTo(myVal) >= 1) {
							log.warning("-Compare Results--------"+ limitVal.compareTo(myVal));
				
							throw new AdempiereException("Price limit violation");
						}
					}
				}

			}
		}

	}

}